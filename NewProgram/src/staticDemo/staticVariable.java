package staticDemo;

public class staticVariable {
	
	static int x=455;  // static variable
	        int y=546546; // instance variable
	        

	public static void main(String[] args) {
		System.out.println(staticVariable.x);  //no need object to call static variable
		
		staticVariable st = new staticVariable ();
		 System.out.println(st.y); // need object to call instance variable
	}

}
