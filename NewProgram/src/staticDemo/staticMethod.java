package staticDemo;

public class staticMethod {

	void play()  //regular method
	{
		System.out.println("I play tennis..");
	}
	//static method
	static void sleep()
	{
		System.out.println("I sleep at 9.."); //no need to create object to call static method
	}
	public static void main(String[] args) {
		sleep();
		staticMethod sm = new staticMethod();
		sm.play();
	}

}
