package basics;

public class Datatypes {

	public static void main(String[] args) {
			
		// different datatypes.. char byte short int long float double boolean
			
		    int a =20;    // initialized 'a' variable and assigned value '20'e
			byte b = 65;
			short s =200;
			long l =46465454;
			
			float f= 6546.22f;
			double d = 456456566.455d;
			
			System.out.println(" Print a value: " +a);
			System.out.println("Print b value :" +b);
			System.out.println("Print s value :" +s);
			System.out.println("Print l value :" +l);
			System.out.println("Print f value :" +f);
			System.out.println("Print d  value :" +d);
			
			

	}

}
