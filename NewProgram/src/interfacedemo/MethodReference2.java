package interfacedemo;

interface DisplayInterface
{
    void display();
}
public class MethodReference2 
{
public void sayHello()
{
    System.out.println("This is Method Reference..");
}
    public static void main(String[] args) 
    {
    MethodReference2 obj = new MethodReference2();
    DisplayInterface dis = obj::sayHello;
    dis.display();
    }

}
