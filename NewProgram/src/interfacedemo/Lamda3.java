package interfacedemo;

@FunctionalInterface
interface Car
{
	
	// abstract method 
	public double method1print(String start, String stop, int km, double rupees);
  }

 public class Lamda3 
 {

	public static void main(String[] args) 
	{
		   Car a = (start,stop,km,rupees)->
		   {
			 System.out.println("Car travelled from "+start+" to " +stop+"");
			 System.out.println("Total cost of trip is ");
			 return (km*rupees);
			   }; 
			System.out.println(a.method1print("Banglore","Hyderabad",13000,600)); 
		}
  }


