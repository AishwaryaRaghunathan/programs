package interfacedemo;

import java.util.ArrayList;
import java.util.List;

public class ForEach {

   public static void main(String[] args) {
        List<String> demo = new ArrayList<>();
        demo.add("Apple");
        demo.add("Orange");
        demo.add("Kiwi");
        demo.add("Banana");
        demo.add("Another fruit");
        demo.forEach(name->System.out.println(name));
    }

}
