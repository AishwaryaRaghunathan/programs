package interfacedemo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDemo1
{

   public static void main(String[] args) 
   {
        List<String> names = Arrays.asList("Aishwarya","Aishu","Aish");
        List<String>othernames=names.stream().filter(n->!"San".equals(n))
                .collect(Collectors.toList());
        othernames.forEach(System.out::println);
    }

}
