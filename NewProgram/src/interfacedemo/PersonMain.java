package interfacedemo;

import java.util.*;

public class PersonMain
{

   public static void main(String[] args)
    {
        //Storing data in list
        List<Person> persons = Arrays.asList(
                new Person("Aishwarya",24),
                new Person("Usha",23),
                new Person("Raghu",24),
                new Person("Prajay",22));
        
        //Convert to Streams //Single Condition
        Person SC = persons.stream().filter(n->"Aishwarya".equals(n.getName())).findAny() .orElse(null);
        System.out.println(SC);
        
        //Convert to Streams //Multiple Condition
        Person MC = persons.stream().filter((n2)->!"Aishwarya".equals(n2.getName()) && 23 == n2.getAge()).findAny() .orElse(null);
        System.out.println(MC);
        
    }

}
