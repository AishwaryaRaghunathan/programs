package interfacedemo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


public class StreamDemo {
        public static void main(String[] args)
        {
    List<String> names = new ArrayList<>();
    names.add("Aishwarya");
    names.add("Ranya");
    names.add("Teju");
    names.add("Divya");
    names.add("Disha");
    names.add("Varsha");


   //streams and lambda
    long count = names.stream().filter(str->str.length()<5).count();
    System.out.println(count+" names with less than 5 characters");


   // creating a stream using Stream.of
    Stream<String> names2 = Stream.of("Aishwarya","Varsha");
    names2.forEach(System.out::println);
    
    }
 }
