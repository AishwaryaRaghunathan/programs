package interfacedemo;

interface AnotherName
{
    void getName(String name);
}
public class MethodReference 
{

   public static void main(String[] args) {
        AnotherName ref =(String s)->System.out.println(s);
        ref.getName("Getting name using lamda");
    }



}

