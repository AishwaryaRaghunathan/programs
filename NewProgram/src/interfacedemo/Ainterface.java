package interfacedemo;

interface Demo 
{
    void method1print();
    
    static void method2show()
    {
        System.out.println(" Static method");
      }
    default void method3() 
    {
        System.out.println(" Default method");
      }
	public class Ainterface implements Demo
	{
	
	@Override
	public void method1print() 
	{
	    // TODO Auto-generated method stub
	    System.out.println("Hello");
	    
	}
		public static void main(String[] args)
		{
		    Ainterface obj = new Ainterface();
		    obj.method1print();
		    Demo.method2show();
		    obj.method3();
        }

     }
}
