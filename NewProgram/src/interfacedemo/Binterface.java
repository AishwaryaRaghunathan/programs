package interfacedemo;

@FunctionalInterface
interface Demo2
{
	   void method1print();    
     static void method2show() 
     {  
     	System.out.println("Static method ");
     }
  }
public class Binterface {

	public static void main(String[] args)
	{
		Demo2 my=()->
         	System.out.println("Expression is !!");
         my.method1print();  
	}
}

