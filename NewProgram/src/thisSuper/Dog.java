package thisSuper;

class Animal{
	String color="White..Parent class"; //this is parent class
}

public class Dog extends Animal{

	String color="Green..";  //current or parent class instance variable child class instance variable
	void printcolor()
	{
		String color="Black..";  //local variable
		System.out.println(color);  //it print local variable
		System.out.println(this.color); // it prints a current class instance variable
		System.out.println(super.color); // it prints a current class instance variable
		
	}
	public static void main(String[] args) {
		Dog d = new Dog();
		d.printcolor();
	}

}
