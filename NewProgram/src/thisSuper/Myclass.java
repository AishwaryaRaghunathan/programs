package thisSuper;

class MyParent{
	String name = "This is Parent class instance variable..";
}

public class Myclass extends MyParent {
String name = "This is instance variable";
    
 void somemethod() 
 {
	 String name = "This is local variable..because I am inside method";
	 System.out.println(name);
	 System.out.println(this.name);
	 System.out.println(super.name);
 }
	public static void main(String[] args) {
		Myclass mc =new Myclass();
		mc.somemethod();
	}

}
