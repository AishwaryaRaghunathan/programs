package Inferface;

//Multiple inheritance is achieved with interface
interface State
{
     void StateName(); //abstract method
}
interface City
{
     void CityName(); //abstract method
}
interface Area
{
     void AreaName(); //abstract method
}
public class Multi implements State, City, Area 
{
     public void StateName()
     {
           System.out.println("Karnataka");
     }
     public void CityName()
     {
           System.out.println("Mysore");
     }
     public void AreaName() 
     {
           System.out.println("Bogadi");
     }
     public static void main(String [] args)
     {
           Multi m = new Multi();
           m.StateName();
           m.CityName();
           m.AreaName();
     }
}


