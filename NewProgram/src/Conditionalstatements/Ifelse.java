package Conditionalstatements;

import java.util.Scanner;

public class Ifelse {
	
			public static void main(String[] args) {
				// if-else conditional statements
				
				Scanner sc = new Scanner(System.in);
				System.out.println("Enter the age of candidate  :");
				int age = sc.nextInt();	
				
				
				if(age>18 && age<100) {  // condition is true so this block of code is goind execute
					System.out.println(" Ok This guy is eligible to vote..");
				}
				
				else if (age>100){
					System.out.println("you are too old so not eligible... just die");
				}
				else {
					System.out.println("You are a minor so not eligible to vote ");
				}

			}

	}
