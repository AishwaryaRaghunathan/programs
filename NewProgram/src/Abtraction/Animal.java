package Abtraction;

public abstract class Animal { //its abstract class..
  //abstract method creation
	abstract void method1();  //no method body
	void regularMethod()      //regular method
	{
		System.out.println("This is my regular method..");
	}
	public static void main(String[] args) {
		
	}

}
