package Abtraction;

public class Main {

	public static void main(String[] args) {
		
		Dog d = new Dog();
		d.method1();  //calling abstract method by using child class object
	}

}
