package Polymorphism;
//method overloading

public class Addition {
	void sum(int a,int b) { // method created with two parameters
		int c = a+b;
		System.out.println("Total of a and b is :" +c);
	}

	//Overloading method 'sum'
	void sum(int a, int b, int c) //three parameters...so overloadded 'sum' method
	{
		int d = a+b+c;
		System.out.println("The total a b and c is :"+d);
	}
	//overloading by changing data types
	void sum(double dd,double ff)
	{
		double rr = dd*ff;
		System.out.println("Product of dd and ff is :" +rr);
	}
	public static void main(String[] args) {
		Addition obj = new Addition(); // object created
		obj.sum(55,66); //two arguments
		obj.sum(20,30,400); //three arguments
	    obj.sum(55.65,67.680); //two different parameters..double of int
	}

}
